const PREC = {
  mult: 7,
  add: 6,
  pipe: 5,
  compare: 4,
  equal: 3,
  and: 2,
  or: 1,
};

module.exports = grammar({
  name: "gleam",
  extras: ($) => [/\s/, $.comment],

  rules: {
    source_file: ($) => repeat($._statement),

    // main module loop
    _statement: ($) =>
      choice(
        $.import_statement,
        $.constant_statement,
        $.custom_type_statement,
        $.type_alias_statement,
        $.external_type_statement,
        $.external_function_statement,
        $.function_statement
      ),

    // Function
    function_statement: ($) =>
      seq(
        optional($.visibility_modifier),
        "fn",
        field("name", $._identifier),
        field("parameters", $._function_parameters),
        field("type", optional(seq("->", $._type_annotation))),
        field("block", seq("{", repeat(choice($.binding, $._expression)), "}"))
      ),

    binding: ($) => seq($.binding_type, $._pattern, "=", $._expression),

    binding_type: ($) => choice("let", "assert", "try"),

    _expression: ($) => choice($._value, $.binary_expression),

    binary_expression: ($) => {
      const table = [
        [PREC.or, "||"],
        [PREC.and, "&&"],
        [PREC.equal, choice("==", "!=")],
        [PREC.compare, choice("<", "<=", ">", ">=", "<.", "<=.", ">.", ">=.")],
        [PREC.pipe, "|>"],
        [PREC.add, choice("+", "-", "+.", "-.")],
        [PREC.mult, choice("*", "/", "*.", "/.", "%")],
      ];

      return choice(
        ...table.map(([precedence, operator]) =>
          prec.left(
            precedence,
            seq(
              field("left", $._expression),
              field("operator", operator),
              field("right", $._expression)
            )
          )
        )
      );
    },

    // External Function
    external_function_statement: ($) =>
      seq(
        optional($.visibility_modifier),
        "external",
        "fn",
        field("name", $._identifier),
        field("parameters", $._function_parameters),
        field("type", optional(seq("->", $._type_annotation))),
        "=",
        field("value", seq($.string_literal, $.string_literal))
      ),

    // Constants
    constant_statement: ($) =>
      seq(
        optional($.visibility_modifier),
        "const",
        field("name", $._identifier),
        field("type", optional(seq(":", $._type_annotation))),
        "=",
        field("value", $._value)
      ),

    // Values
    _value: ($) =>
      choice(
        // a
        $._identifier,
        // a.a
        $.qualified_identifier,
        // A
        $._type_identifier,
        // a.A
        $.qualified_type_identifier,
        // a(..), a.a(..), A( .. ), a.A(..),
        $.function_call,
        $.integer_literal,
        $.float_literal,
        // string
        $.string_literal,
        // tuple
        $.tuple_literal,
        // list
        $._list_literals,
        // bitstring
        $.bitstring_literal,
        // group
        seq("{", repeat($._expression), "}"),
        // case
        $.case_expression
      ),

    qualified_type_identifier: ($) =>
      seq($._module_identifier, ".", $._type_identifier),

    qualified_identifier: ($) => seq($._module_identifier, ".", $._identifier),

    function_call: ($) =>
      seq(
        choice(
          $._identifier,
          $.qualified_identifier,
          $._type_identifier,
          $.qualified_type_identifier
        ),
        field("arguments", $._function_arguments)
      ),

    // Imports
    import_statement: ($) =>
      seq(
        "import",
        field("module", $.module_path),
        field(
          "imports",
          optional(seq(".", "{", sepBy(",", $.imported_field), "}"))
        ),
        optional(seq("as", field("as_name", $._module_identifier)))
      ),

    imported_field: ($) =>
      seq(
        field("name", choice($._type_identifier, $._identifier)),
        optional(seq("as", field("as_name", $._identifier)))
      ),

    // ex: a/b/c
    module_path: ($) => sepBy1NoTrail("/", $._module_identifier),

    _type_identifier: ($) => alias($.up_name, $.type_identifier),
    _identifier: ($) => alias($.down_name, $.identifier),
    _discard_identifier: ($) => alias($.discard_name, $.discard_identifier),
    _module_identifier: ($) => alias($.down_name, $.module_identifier),
    _type_hole: ($) => alias($.discard_name, $.type_hole),

    // Type creation
    type_alias_statement: ($) =>
      seq(
        optional($.visibility_modifier),
        "type",
        field("name", $._type_identifier),
        field("type_parameters", optional($.type_parameters)),
        seq("=", field("type", $._type_annotation))
      ),

    custom_type_statement: ($) =>
      seq(
        optional($.visibility_modifier),
        optional($.opacity_modifier),
        "type",
        field("name", $._type_identifier),
        field("type_parameters", optional($.type_parameters)),
        field(
          "constructors",
          seq("{", repeat($.type_constructor_statement), "}")
        )
      ),

    type_constructor_statement: ($) =>
      seq(
        field("name", $._type_identifier),
        field(
          "parameters",
          optional(
            seq(
              "(",
              sepBy1(",", choice($.named_parameter, $.anonymous_parameter)),
              ")"
            )
          )
        )
      ),

    external_type_statement: ($) =>
      seq(
        optional($.visibility_modifier),
        $.external_modifier,
        "type",
        field("name", $._type_identifier),
        field("type_parameters", optional($.type_parameters))
      ),

    // ex: (a, b)
    type_parameters: ($) =>
      prec(1, seq("(", sepBy1(",", $._type_variable), ")")),

    // ex: a
    _type_variable: ($) => alias($.down_name, $.type_variable),

    // Type annotation
    _type_annotation: ($) =>
      choice(
        // A
        $._type_identifier,
        // A(..)
        $.generic_type,
        // a
        $._type_variable,
        // a.A
        seq($._module_identifier, ".", $._type_identifier),
        // a.A(..)
        seq($._module_identifier, ".", $.generic_type),
        // tuple
        seq("tuple", "(", sepBy1(",", $._type_annotation), ")"),
        // function
        seq(
          "fn",
          "(",
          sepBy1(",", $._type_annotation),
          ")",
          "->",
          $._type_annotation
        ),
        // hole
        $._type_hole
      ),

    // ex: A(a, b)
    generic_type: ($) =>
      seq(
        $._type_identifier,
        "(",
        sepBy1(",", $._type_annotation),
        optional(","),
        ")"
      ),

    // Patterns
    _pattern: ($) =>
      choice(
        $._identifier,
        $._discard_identifier,
        $.integer_literal,
        $.float_literal,
        $.string_literal,
        $.tuple_literal,
        $.list_literal,
        $.bitstring_literal,
        // A, A(..), a.A, a.A(..)
        seq(
          optional(seq($._module_identifier, ".")),
          $._type_identifier,
          optional($._function_arguments)
        )
      ),

    // Case
    case_expression: ($) =>
      seq(
        "case",
        field("inputs", sepBy1NoTrail(",", $._value)),
        "{",
        field("branches", repeat($.case_branch)),
        "}"
      ),

    case_branch: ($) =>
      seq(
        sepBy1NoTrail("|", sepBy1NoTrail(",", $._pattern)),

        optional($.case_guard),

        "->",
        $._value
      ),

    case_guard: ($) => seq("if", $.binary_expression),

    // Literals
    string_literal: ($) => seq('"', repeat(token(prec(1, /\\.|[^"]/))), '"'),

    bitstring_literal: ($) =>
      seq("<<", field("segments", sepBy(",", $.bitstring_segment)), ">>"),

    bitstring_segment: ($) =>
      seq(
        field("pattern", $._pattern),
        field(
          "options",
          optional(
            seq(
              ":",
              choice(
                $.integer_literal,
                sepBy1NoTrail(
                  "-",
                  seq($._identifier, optional(seq("(", $.integer_literal, ")")))
                )
              )
            )
          )
        )
      ),

    integer_literal: ($) =>
      choice(
        // decimal
        /-?[0-9][0-9_]*/,
        // octal
        /0[oO][0-7][_0-7]*/,
        // binary
        /0[bB][01][01_]*/,
        // hex
        /0[xX][0-9A-Fa-f][0-9A-Fa-f_]*/
      ),

    float_literal: ($) => /-?(0\.|[1-9][0-9_]*\.)[0-9][0-9_]*/,

    tuple_literal: ($) => seq("tuple", "(", sepBy(",", $._value), ")"),

    _list_literals: ($) => choice($.empty_list_literal, $.list_literal),

    empty_list_literal: ($) => prec(1, seq("[", "]")),

    list_literal: ($) =>
      seq(
        "[",
        sepBy(",", $._value),

        optional(seq($.spread, optional(","))),
        "]"
      ),
    spread: ($) => seq("..", $._value),

    // Generic Structure
    _function_arguments: ($) =>
      seq(
        "(",
        sepBy(
          ",",
          choice($.named_argument, $.anonymous_argument, $.argument_hole)
        ),
        ")"
      ),

    named_argument: ($) =>
      seq(field("name", $._identifier), ":", field("value", $._value)),

    anonymous_argument: ($) => field("value", $._value),

    argument_hole: ($) => field("value", $._discard_identifier),

    _function_parameters: ($) =>
      seq("(", sepBy(",", $._function_parameter), optional(","), ")"),

    _function_parameter: ($) =>
      choice($.named_parameter, $.anonymous_parameter, $.labeled_parameter),

    // ex: a: A(a), _: A
    named_parameter: ($) =>
      seq(
        field("name", choice($._identifier, $._discard_identifier)),
        ":",
        field("type", $._type_annotation)
      ),

    // ex: A(a)
    anonymous_parameter: ($) => field("type", $._type_annotation),

    // ex: a b, a b: C, a _: C, _ _: C
    labeled_parameter: ($) =>
      seq(
        field("label", $._identifier),
        field("name", choice($._identifier, $._discard_identifier)),
        field("type", optional(seq(":", $._type_annotation)))
      ),

    // Special keywords
    visibility_modifier: ($) => prec.right("pub"),
    opacity_modifier: ($) => prec.right("opaque"),
    external_modifier: ($) => prec.right("external"),

    // Comments
    comment: ($) => token(seq("//", /.*/)),

    // name parsers, don't use these directly, create an alias
    up_name: ($) => /[A-Z][a-zA-Z\d_]*/,
    down_name: ($) => /[a-z][a-z\d_]*/,
    discard_name: ($) => /[_][a-z\d_]*/,
  },
});

// series of rules seperated by a separator, at least 1 value
// trailing sep is allowed
function sepBy1(sep, rule) {
  return seq(rule, repeat(seq(sep, rule)), optional(sep));
}

// series of rules seperated by a separator, at least 1 value
// trailing sep is not allowed
function sepBy1NoTrail(sep, rule) {
  return seq(rule, repeat(seq(sep, rule)));
}

// series of rules seperated by a separator, maybe no values
function sepBy(sep, rule) {
  return optional(sepBy1(sep, rule));
}
