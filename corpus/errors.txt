=====================
Opaque Type Alias 
=====================

pub opaque type A = B

---

(source_file
  (ERROR (visibility_modifier) (opacity_modifier) (type_identifier) (type_identifier))
)

=====================
Unescaped quotes 
=====================

const a = """
const a = "\\""

---

(source_file
  (constant_statement (identifier) (string_literal)) (ERROR)
  (constant_statement (identifier) (string_literal)) (ERROR)
)



